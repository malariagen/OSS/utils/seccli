# Seccli - AWS Secrets Manager CLI

Seccli is a lightweight, standalone command-line application to interact with AWS Secrets Manager. This application is designed as a minimalistic tool for retrieving secrets from AWS Secrets Manager, providing a reduced footprint compared to the official AWS CLI.

## Disclaimer

While Seccli offers some functionalities for interacting with AWS Secrets Manager, it's worth noting that the official AWS CLI provides a comprehensive set of utilities and should be used for most production use cases. Seccli is designed for specific use cases where a smaller binary is desired.

## Features

- Retrieve a secret from AWS Secrets Manager
- Optionally select a specific key from a secret's JSON object
- List all secrets in AWS Secrets Manager
- Base64 decode the output
- Choose a specific AWS region

## Installation
seccli is a standalone application and should not require any additional dependencies. You can download the release version that matches your target OS/Arch from the [release page](https://gitlab.com/malariagen/OSS/utils/seccli/-/releases) and start using it. It is able to read AWS config and credentials from the default AWS locations or environment variables.

Alternatively, the code can be downloaded and built into the same binary on any target platform that supports the go compiler.

## Usage

To use Seccli, first build the application with the `go build` command:

```bash
go build -o seccli
```

This will create an executable named seccli in your current directory. You can now use Seccli to interact with AWS Secrets Manager:

- Retrieve a secret: ./seccli <secretname>
- Retrieve a secret from a specific AWS region: ./seccli -r <region> <secretname>
- Retrieve a value from a specific key in a secret: ./seccli -k <key> <secretname>
- List all secrets: ./seccli -l
- Base64 decode the output: ./seccli -b -k password <secretname>

Replace `<secretname>` with the name of the secret you want to retrieve, `<region>` with the name of the AWS region you want to interact with, and <key> with the key you want to select from the secret's JSON object.

## Examples
```bash
# Retrieve a secret
./seccli mysecret

# Retrieve a secret from a specific AWS region
./seccli -r us-west-2 mysecret

# Retrieve a value from a specific key in a secret
./seccli -k mykey mysecret

# List all secrets
./seccli -l

# Base64 decode the output
./seccli -k password -b mysecret
```

Please make sure your AWS credentials are properly configured. You can set them up either by exporting them as environment variables or by storing them in the AWS configuration file.

## License
Seccli is Open Source software released under the Apache 2.0 license.

