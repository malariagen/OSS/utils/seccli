package main

import (
	"encoding/base64"
	"encoding/json"
	"flag"
	"fmt"
	"strconv"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/secretsmanager"
)

func main() {
	var region string
	var listSecrets bool
	var key string
	var base64Decode bool

	flag.StringVar(&region, "r", "", "AWS Region")
	flag.BoolVar(&listSecrets, "l", false, "List all secrets")
	flag.StringVar(&key, "k", "", "Key to select from the secret's JSON object")
	flag.BoolVar(&base64Decode, "b", false, "Base64 decode the final output")
	flag.Parse()

	sessOpts := session.Options{
		SharedConfigState: session.SharedConfigEnable,
	}

	if region != "" {
		sessOpts.Config = aws.Config{Region: aws.String(region)}
	}

	sess, err := session.NewSessionWithOptions(sessOpts)
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	svc := secretsmanager.New(sess)

	if listSecrets {
		result, err := svc.ListSecrets(&secretsmanager.ListSecretsInput{})
		if err != nil {
			fmt.Println(err.Error())
			return
		}

		for _, secret := range result.SecretList {
			fmt.Println(*secret.Name)
		}
	} else {
		if len(flag.Args()) != 1 {
			fmt.Println("You must provide a secret name as argument.")
			return
		}

		secretName := flag.Arg(0)

		input := &secretsmanager.GetSecretValueInput{
			SecretId: aws.String(secretName),
		}

		result, err := svc.GetSecretValue(input)
		if err != nil {
			fmt.Println(err.Error())
			return
		}

		output := *result.SecretString

		if key != "" {
			var jsonData map[string]interface{}
			err := json.Unmarshal([]byte(output), &jsonData)
			if err != nil {
				fmt.Println("Error parsing JSON:", err)
				return
			}

			value, ok := jsonData[key]
			if !ok {
				fmt.Println("Key not found in secret.")
				return
			}

			// Directly convert the value to a string, if possible.
			switch v := value.(type) {
			case string:
				output = v
			case float64:
				output = strconv.FormatFloat(v, 'f', -1, 64)
			case bool:
				output = strconv.FormatBool(v)
			default:
				fmt.Println("Cannot convert value to string.")
				return
			}
		}

		if base64Decode {
			data, err := base64.StdEncoding.DecodeString(output)
			if err != nil {
				fmt.Println("Error decoding base64:", err)
				return
			}

			output = string(data)
		}

		fmt.Println(output)
	}
}

